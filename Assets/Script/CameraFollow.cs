﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    SplineFollower spline;

    private void Awake()
    {
        spline = gameObject.GetComponent<SplineFollower>();
    }
    public void StartGame()
    {
        StartCoroutine(ExecuteAfterTime(0.15f));
    }

    public void FinishGame()
    {
        spline.autoFollow = false;
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        spline.Restart(0);
        spline.autoFollow = true;
    }
}

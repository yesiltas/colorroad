﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBarrier : MonoBehaviour
{
    SplineComputer computer;

    public void Initialize(Vector3 position, Quaternion rotation)
    {
        SetPosition(position, rotation);
    }

    private void SetPosition(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBarrier : MonoBehaviour
{
    Renderer rend;
    GameColors gameColors;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public void Initialize(Vector3 position, Quaternion rotation, GameColors gameColors)
    {
        SetColor(gameColors);
        SetPosition(position, rotation);
    }

    private void SetColor(GameColors gameColors)
    {
        switch (gameColors)
        {
            case GameColors.red:
                rend.material.SetColor("_Color", Color.yellow);
                this.gameColors = GameColors.yellow;
                break;
            case GameColors.yellow:
                rend.material.SetColor("_Color", Color.green);
                this.gameColors = GameColors.green;
                break;
            case GameColors.green:
                rend.material.SetColor("_Color", Color.red);
                this.gameColors = GameColors.red;
                break;
        }
        GameManager.instance.BarrierColor(this.gameColors);
    }

    private void SetPosition(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
    }

    public GameColors GetColor()
    {
        return gameColors;
    }
}

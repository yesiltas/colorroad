﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBarierManager : MonoBehaviour
{
    public GameObject Prefab;
    List<ColorBarrier> barriers = new List<ColorBarrier>();

    public void AgainGame()
    {
        foreach (var item in barriers)
        {
            Destroy(item.gameObject);
        }
        barriers.Clear();
    }

    public void InstantiateBarrier(Vector3 position, Quaternion rotation, GameColors gameColors)
    {
        ColorBarrier barrier = Instantiate(Prefab).GetComponent<ColorBarrier>();
        barrier.Initialize(position, rotation, gameColors);
        barriers.Add(barrier);
    }

    public GameColors GetColor(ColorBarrier barrier)
    {
        return barrier.GetColor();
    }


}

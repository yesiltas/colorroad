﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject finishPanel;
    public GameObject gamePanel;
    public Text startText;
    public Text finishText;
    public Text scoreText;
    public Text winLoseText;
    int score ;

    private void Awake()
    {
        startText.text = "Tap To Start";
        finishText.text = "Play Again";
    }

    private void Start()
    {
        GameManager.instance.PlayerScore += PlayerScore;
        GameManager.instance.PlayerDie += PlayerDie;
        GameManager.instance.PlayerWin += PlayerWin;
    }

    public void SetScoreText()
    {
        score++;
        scoreText.text = "Score : " + score.ToString();
    }

    public void StartGame()
    {
        score = -1;
        GameManager.instance.StartGame();    
        OpenGamePanel();
    }

    public void Again()
    {
        OpenStartPanel();
        GameManager.instance.Again();
    }
    
    private void PlayerDie()
    {
        winLoseText.text = "You Lose";
        OpenFinishPanel();
    }

    private void PlayerScore()
    {
        SetScoreText();
    }

    private void PlayerWin()
    {
        winLoseText.text = "You Win";
        OpenFinishPanel();
    }

    private void OpenStartPanel()
    {
        startPanel.SetActive(true);
        finishPanel.SetActive(false);
        gamePanel.SetActive(false);
    }

    private void OpenFinishPanel()
    {        
        startPanel.SetActive(false);
        finishPanel.SetActive(true);
        gamePanel.SetActive(false);
    }

    private void OpenGamePanel()
    {
        
        SetScoreText();
        startPanel.SetActive(false);
        finishPanel.SetActive(false);
        gamePanel.SetActive(true);
    }
}


﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineManager : MonoBehaviour
{
    public List<GameObject> PreFabRoad = new List<GameObject>();
    public List<GameObject> PreFabCross = new List<GameObject>();
    SplineComputer computer;
    public GameObject prefabBarrier;
    List<int> pointPercent = new List<int>();
    List<SplineComputer> splines = new List<SplineComputer>();
    RoadBarrier roadBarrier;

    private void Awake()
    {
        computer = GetComponent<SplineComputer>();
    }

    public void StartGame()
    {
        SetRoad();
    }

    public void AgainGame()
    {
        foreach (var item in splines)
        {
            Destroy(item.gameObject);
        }
        splines.Clear();
        pointPercent.Clear();
        Destroy(roadBarrier.gameObject);
    }

    public void SetRoad()
    {
        int pointIndex = 0;
        Vector3 startPosition = Vector3.zero;
        int createdWayCount = 3;
        for (int i = 0; i < createdWayCount; i++)
        {
            int randomIndex = UnityEngine.Random.Range(0, PreFabRoad.Count);
            SplineComputer splineRoad = Instantiate(PreFabRoad[randomIndex]).GetComponent<SplineComputer>();
            splines.Add(splineRoad);
            SetPoints(ref pointIndex, ref startPosition, splineRoad, true);
            if (i < createdWayCount - 1)
                SetPoints(ref pointIndex, ref startPosition, PreFabCross[UnityEngine.Random.Range(0, PreFabCross.Count)].GetComponent<SplineComputer>(), false);
            else
                InstantiateFinishBarrier(computer.GetPoint(computer.pointCount - 1).position, computer.Evaluate(computer.pointCount - 1).rotation);
        }
        computer.Rebuild();
        computer.precision = 0.99;
        GameManager.instance.SetColorBalls(computer, pointPercent);
    }

    public void InstantiateFinishBarrier(Vector3 position, Quaternion rotation)
    {
        roadBarrier = Instantiate(prefabBarrier).GetComponent<RoadBarrier>();
        roadBarrier.Initialize(position, rotation);
    }

    private void SetPoints(ref int pointIndex, ref Vector3 startPosition, SplineComputer splineRoad, bool isRoad)
    {
        SplinePoint[] splinePoints = splineRoad.GetPoints();
        Vector3 firstPosition = splinePoints[0].position;
        Vector3 dif = startPosition - firstPosition;
        Vector3 difRoad = startPosition - splineRoad.GetPointPosition(0);
        for (int i = 0; i < splinePoints.Length; i++)
        {
            if (computer.pointCount <= pointIndex)
                computer.SetPoint(pointIndex, splinePoints[i]);
            computer.SetPointPosition(pointIndex, splinePoints[i].position + dif);
            if (isRoad)
            {
                if (i != 0 && i != splinePoints.Length - 1)
                    pointPercent.Add(pointIndex);
            }
            pointIndex++;
        }
        if (isRoad)
        {
            for (int i = 0; i < splineRoad.pointCount; i++)
            {
                splineRoad.SetPointPosition(i, splinePoints[i].position + difRoad);
            }
            splineRoad.Rebuild();
        }

        startPosition = computer.GetPointPosition(pointIndex-1);
    }
}

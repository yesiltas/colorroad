﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Action PlayerScore;
    public Action PlayerDie;
    public Action PlayerWin;
    public ColorBallManger ballManger;
    public ColorBarierManager colorBarrierManager;
    public Player player;
    public SplineManager splineManager;
    public CameraFollow CameraFollow;
    GameColors gameColors;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        player.PlayerScore += PlayerHitScore;
        player.PlayerDie += PlayerHitDie;
    }

    public void StartGame()
    {       
        splineManager.StartGame();
        player.gameObject.SetActive(true);
        player.StartGame();
        CameraFollow.StartGame();
    }

    void FinishGame()
    {
        CameraFollow.FinishGame();
        player.FinishGame();
    }

    public void Again()
    {
        splineManager.AgainGame();
        ballManger.AgainGame();
        colorBarrierManager.AgainGame();
    }

    //Player Functions
    private void PlayerHitDie()
    {
        PlayerDie();
        FinishGame();
    }

    public void PlayerHitScore(ColorBallController colorBall)
    {
        PlayerScore();
        Destroy(colorBall.gameObject);
    }

    public void PlayerWinGame()
    {
        PlayerWin();
        FinishGame();
    }

    //ColorBall Functions
    public void SetColorBalls(SplineComputer computer, List<int> pointPercent)
    {
        GameColors gameColors = player.PlayerColorIndex();
        ballManger.InstantiateColorBall(computer, pointPercent, gameColors);
    }

    //Barrier Functions
    public void SetColorBarrier(Vector3 position, Quaternion rotation, GameColors gameColors)
    {
        colorBarrierManager.InstantiateBarrier(position, rotation, gameColors);
    }

    public GameColors GetBarrierColor(ColorBarrier barrier)
    {
        return colorBarrierManager.GetColor(barrier);
    }

    public void BarrierColor(GameColors gameColors)
    {
        this.gameColors = gameColors;
    }

    public GameColors GetBarrierColor()
    {
        return this.gameColors;
    }
}

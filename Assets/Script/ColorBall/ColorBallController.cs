﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBallController : MonoBehaviour
{
    Renderer rend;
    GameColors gameColors;
    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public void Initialize(Vector3 position, Quaternion rotation)
    {
        gameObject.transform.rotation = rotation;
        gameObject.transform.position = position;
    }

    public void SetColor(GameColors colors)
    {
        switch (colors)
        {
            case GameColors.red:
                rend.material.SetColor("_Color", Color.red);
                gameColors = GameColors.red;
                break;
            case GameColors.yellow:
                rend.material.SetColor("_Color", Color.yellow);
                gameColors = GameColors.yellow;
                break;
            case GameColors.green:
                rend.material.SetColor("_Color", Color.green);
                gameColors = GameColors.green;
                break;
        }
    }

    public GameColors GetColor()
    {
        return gameColors;
    }
}

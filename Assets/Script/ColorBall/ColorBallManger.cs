﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBallManger : MonoBehaviour
{
    public GameObject prefab;
    public ColorBallManger ballManger;
    List<ColorBallGroupParent> colorBallGroups = new List<ColorBallGroupParent>();

    public void AgainGame()
    {
        foreach (var item in colorBallGroups)
        {
            Destroy(item.gameObject);
        }
        colorBallGroups.Clear();
    }

    public void InstantiateColorBall(SplineComputer computer, List<int> pointPercent, GameColors gameColors)
    {
        for (int i = 0; i < pointPercent.Count - 1; i += 2)
        {
            if (pointPercent[i + 1] - pointPercent[i] < 5)
            {
                for (double j = computer.GetPointPercent(pointPercent[i]); j <= computer.GetPointPercent(pointPercent[i + 1]); j += 0.01)
                {
                    Vector3 position = computer.EvaluatePosition(j);
                    Quaternion rotation = computer.Evaluate(j).rotation;
                    ColorBallGroupParent colorBallGroup = Instantiate(prefab).GetComponent<ColorBallGroupParent>();
                    colorBallGroup.Initialize(position, rotation);
                    colorBallGroups.Add(colorBallGroup);
                }
            }

            if (i < pointPercent.Count - 3)
            {
                if (pointPercent[i + 2] - pointPercent[i + 1] < 5)
                {
                    double barrierPercent = (computer.GetPointPercent(pointPercent[i + 2]) + computer.GetPointPercent(pointPercent[i + 1])) * 0.5;
                    GameManager.instance.SetColorBarrier(computer.EvaluatePosition(barrierPercent), computer.Evaluate(barrierPercent).rotation, gameColors);
                    gameColors = GameManager.instance.GetBarrierColor();
                }
            }
        }
    }
}

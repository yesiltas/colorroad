﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBallGroupParent : MonoBehaviour
{
    public GameObject prefab;

    public void Initialize(Vector3 position, Quaternion rotation)
    {
        List<GameColors> ballColor = CreateColor();
        for (int i = 0; i < 3; i++)
        {
            ColorBallController colorBall = Instantiate(prefab).GetComponent<ColorBallController>();
            colorBall.transform.parent = gameObject.transform;
            colorBall.transform.rotation = rotation;
            colorBall.SetColor(ballColor[i]);
        }
        transform.GetChild(0).transform.position = new Vector3(transform.localPosition.x - 2f, transform.localPosition.y, transform.localPosition.z);
        transform.GetChild(1).transform.position = new Vector3(transform.localPosition.x + 2f, transform.localPosition.y, transform.localPosition.z);
        transform.GetChild(2).transform.position = transform.localPosition;
        transform.position = position;
        transform.rotation = rotation;
    }

    List<GameColors> CreateColor()
    {
        List<GameColors> ballColor = new List<GameColors>();
        for (int i = 0; i < 3;)
        {
            int colorIndex = Random.Range(0, 3);
            switch (colorIndex)
            {
                case 0:
                    if (!(ballColor.Contains(GameColors.red)))
                    {
                        ballColor.Add(GameColors.red);
                        i++;
                    }
                    break;
                case 1:
                    if (!(ballColor.Contains(GameColors.yellow)))
                    {
                        ballColor.Add(GameColors.yellow);
                        i++;
                    }
                    break;
                case 2:
                    if (!(ballColor.Contains(GameColors.green)))
                    {
                        ballColor.Add(GameColors.green);
                        i++;
                    }
                    break;
            }
        }
        return ballColor;
    }
}

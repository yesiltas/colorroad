﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject player;

    public float distance;
    public float height;

    void LateUpdate()
    {
        transform.position = player.transform.position;
        transform.forward = player.transform.forward;
        transform.position -= transform.forward * distance;
        transform.position += Vector3.up * height;
    }
}


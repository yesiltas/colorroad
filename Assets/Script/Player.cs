﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public enum Status
    {
        Wait, Move
    }
    public Status status;
    GameColors gameColors;
    SplineFollower spline;
    Renderer rend;
    float startPosition;
    float currentPosition;
    public Action<ColorBallController> PlayerScore;
    public Action PlayerDie;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
        rend.material.SetColor("_Color", Color.red);
        gameColors = GameColors.red;
        spline = gameObject.GetComponent<SplineFollower>();
    }

    public void StartGame()
    {
        StartCoroutine(ExecuteAfterTime(0.25f));
    }

    public void FinishGame()
    {
        ChangeStatus(Status.Wait);
        transform.position = Vector3.zero;
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        spline.motion.offset = Vector2.zero;
        spline.Restart(0);
        ChangeStatus(Status.Move);
    }

    private void ChangeStatus(Status status)
    {
        this.status = status;
    }

    private void Update()
    {
        switch (status)
        {
            case Status.Wait:
                spline.autoFollow = false;
                break;
            case Status.Move:
                if (Input.GetMouseButtonDown(0))
                {
                    startPosition = Input.mousePosition.x;
                }

                if (Input.GetMouseButton(0))
                {
                    currentPosition = Input.mousePosition.x;
                    swipe();
                }
                spline.autoFollow = true;

                if (Input.GetMouseButtonUp(0))
                {
                    startPosition = 0;
                    currentPosition = 0;
                }
                spline.autoFollow = true;
                break;
        }
    }

    public float sensetive;

    private void swipe()
    {
        float distance;
        float offset;
        distance = (currentPosition - startPosition) * 100 / (Screen.width * 0.75f);
        offset = distance * 0.05f * sensetive;
        offset = Mathf.Clamp(offset+ spline.motion.offset.x, -2.5f, 2.5f);
        spline.motion.offset = new Vector2(offset, 0);
        startPosition = currentPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target")
        {
            if (gameColors == other.GetComponent<ColorBallController>().GetColor())
                PlayerScore(other.GetComponent<ColorBallController>());
            else
                PlayerDie();
        }
        if (other.tag == "Bridge")
        {
            GameManager.instance.PlayerWinGame();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Barrier")
        {
            gameColors = GameManager.instance.GetBarrierColor(other.GetComponent<ColorBarrier>());
            switch (gameColors)
            {
                case GameColors.red:
                    rend.material.SetColor("_Color", Color.red);
                    break;
                case GameColors.yellow:
                    rend.material.SetColor("_Color", Color.yellow);
                    break;
                case GameColors.green:
                    rend.material.SetColor("_Color", Color.green);
                    break;
            }
            
        }

    }

    public GameColors PlayerColorIndex()
    {
        return gameColors;
    }
}